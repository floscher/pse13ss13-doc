# PSE in Summersemester 2013: Project 13 (Routing-app for pedestrians)

We are developing an Android app for pedestrians to plan routes and navigate over them.

## Purpose

This repository shares documentation files (like agendas, logs, designs) or
additional information (like papers, manuals) for the PSE13SS13 project.

**PLEASE DO NOT COMMIT ANY APPLICATION CODE TO THIS REPOSITORY!**
*Use pse13ss13-app instead.*

## Rules

* Use UTF-8 as charset and <lf> as line ending.
* Remove trailing whitespaces or empty lines at the end of file.
  * See Eclipse-Plugin "AnyEdit", which does that work for you everytime you save (http://andrei.gmxhome.de/eclipse/)
* Use only ASCII characters for file and directory names. Do not use umlauts!
* Do not exceed a maximum file size of 10 MiB.

## Best Practices

* Use only English for commit messages.
* Use 4 spaces for indentation instead of tab.
* Normally everything can be commited directly into the master-branch
