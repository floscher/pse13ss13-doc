NAME
    Berechnung von Joggingrouten

    
IDEE
    Basierend auf dem Paper 'Efficient Computation of Jogging Routes' berechnen wir Rundkurse in Karten für Läufer und
    Wanderer. Diese visualisieren wir in Form eines farbigen Weges in einer Karte.
    

UMSETZUNGSDETAILS
    - OSM-Kartenmaterial
    - Vorberechnung für im Paper beschriebene Algorithmen
    - Routenberechung mit Greedy Faces oder Partial Shortest Paths
    - evtl. eigenes Kartenrendering
    - evtl. Navigationsführung wie im Vorschlag Fußgängernavigation (erfordert Android)
    - evtl. Statistikfunktion über reale Strecke, Dauer, Geschwindigkeit (erfordert Android)
    

BESONDERHEITEN
    konzeptionell
        - neue Algorithmen
    
    technisch
        - keine, falls kein eigenes Kartenrendering, Navigations- oder Statistikfunktionen implementiert werden
        
        
SCHWIERIGKEITEN
    - sehr komplizierte Algorithmen
    - hoher Rechenaufwand

    
Umsetzung entspricht nicht den in der Aufgabenstellung geforderten Mindestanforderungen. Hier wäre eine gesonderte
Absprache mit unserem Betreuer nötig.