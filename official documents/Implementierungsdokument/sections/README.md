# Anleitung für das Einstellen der Komponenten
In den Ordner "model" könnt ihr eure Komponenten reinstellen.
Ihr könnt ja das Template unter "model/generic.tex" als Vorlage verwenden:
* Kopiert die Datei einfach "nebendran", benennt sie sinnvoll (nicht nummerieren).
* Tragt sie anschließend bitte in der Datei "model.tex" ein (analog zu generic.tex)

Mit "view" und "controller" geht es alles ganz genauso. Nur dass ich dafür jetzt kein eigenes "generic.tex" erstellt habe.


## Wichtige Hinweise:
* Oberste Hierarchieebene der Überschriften ist "subsection"!
* Für Anführungszeichen bitte entweder "`Lorem ipsum"' verwenden, oder \enquote{Lorem ipsum}.
* Für > und < bitte \\> (ein Backslash und ein Größer-Zeichen) bzw. \\< (ein Backslash und ein Kleiner-Zeichen) verwenden.
* Für _ (Unterstrich) bitte \\_ (ein Backslash und ein Unterstrich) verwenden 
* Für Einträge ins Glossar wie gewohnt \gls{route} oder \glslink{route}{Alternativtext}.
* Es existieren folgende Shortcuts:
	* \m{Hallo Welt!} für Monospace-Font
	* \q{Hallo Welt!} für Anführungszeichen (identisch mit \enquote{hallo Welt!}
	* \itnl[42] für ein Item einer description-Liste, dem ein Zeilenumbruch (newLine) folgt (identisch mit \item[42]~\\)
	* \arr für [] (bitte \arr verwenden, da die andere Schreibweise zu Problemen führen _kann_).
