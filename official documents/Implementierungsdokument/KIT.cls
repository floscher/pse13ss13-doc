\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{KIT}[2004/04/29 The ISAS studienarbeit class]

\ProcessOptions\relax

\LoadClass[12pt,a4paper,version=last,draft,toc=listof,toc=bib,headings=normal,BCOR=0mm,ngerman]{scrartcl}


%-------------------------------------------------------
% Usual packages
%-------------------------------------------------------

\RequirePackage[utf8]{inputenc}
\RequirePackage[ngerman]{babel}
\RequirePackage[babel,german=quotes]{csquotes}
\RequirePackage[T1]{fontenc}
\RequirePackage{lmodern}
\RequirePackage{amsfonts,amsmath,amssymb}%,dsfont}
\RequirePackage{bm} % correct bold symbols, like \bm
\RequirePackage[amsmath,thmmarks,hyperref]{ntheorem}
\RequirePackage{booktabs,tabularx,longtable,multirow}
\RequirePackage[single]{accents} % own accents
\RequirePackage[single]{accents} % own accents  
\RequirePackage[pdftex]{graphicx,color,rotating} % include graphics and color
\RequirePackage[headsepline,plainfootsepline,footsepline]{scrpage2}
\RequirePackage[]{scrpage2}
\RequirePackage[absolute,overlay]{textpos}
\RequirePackage{subfig}
\RequirePackage{algorithmic}
\RequirePackage{algorithm}
%\RequirePackage[backend=bibtex8,sortlocale=de,style=authoryear-icomp,dashed=false,pagetracker=page,citereset=section,doi=false,isbn=false,url=false]{biblatex}
\RequirePackage{epstopdf}
\RequirePackage{wrapfig} % Text umfließt Bild
\RequirePackage{xspace} % Leerzeichen, außer vor einem Punkt


%-------------------------------------------------------
% TikZ and related packages with options
%-------------------------------------------------------

\RequirePackage{tikz}
\usetikzlibrary{positioning,chains,calc,arrows,through,fit,plotmarks,matrix,arrows,topaths}


%-------------------------------------------------------
% KOMA-Script and related options
%-------------------------------------------------------

\setkomafont{descriptionlabel}{\bfseries}

\ihead[]{}
\chead[]{}
\ohead[]{}
\ifoot[]{}
\cfoot[\pagemark]{\pagemark}
\ofoot[]{}
\setheadsepline{0.5pt}
\setfootsepline{0.5pt}
\pagestyle{scrheadings}

\renewenvironment{quotation}{%
  \list{}{\listparindent 1em%
    \itemindent    \listparindent
    \rightmargin   \leftmargin
    \parsep        \z@ \@plus\p@
    \slshape
  }%
  \item\relax
}{%
  \endlist
}

\newcommand{\quotremark}[1]{\textnormal{[Anm.: #1]}}

\let\oldmarginpar\marginpar
\renewcommand\marginpar[1]{\oldmarginpar[\raggedleft\sffamily\scriptsize #1]{\raggedright\sffamily\scriptsize #1}}


%-------------------------------------------------------
% Hyper- and cleverref
%-------------------------------------------------------

\RequirePackage[pdftex,hypertexnames=false,colorlinks,linkcolor=black,citecolor=black]{hyperref} % provide links in pdf
%\RequirePackage[pdftex,unicode,hypertexnames=false]{hyperref}
\hypersetup{pdftitle={Pflichtenheft},
  pdfsubject={Routenplaner zur Fußgängernavigation mit Rundkursberechnung},
  pdfauthor={Ludwig Biermann, Thomas Kadow, Lukas Müller, Florian Schäfer, Matthias Stumpp},
  pdfkeywords={Routenplaner zur Fußgängernavigation mit Rundkursberechnung},
  pdfstartview=Fit,
  pdfpagemode={UseOutlines},% None, FullScreen, UseOutlines
  bookmarksnumbered=true,
  bookmarksopen=false,
  pdfborder={0 0 0},
  pdfpagelayout=TwoColumnRight
}

%\RequirePackage{cleveref}
%\crefname{equation}{Gleichung}{Gleichungen}
%\creflabelformat{equation}{#2(#1)#3}
%\crefname{chapter}{Kapitel}{Kapitel}
%\crefname{section}{Abschnitt}{Abschnitte}
%\crefname{subsection}{Abschnitt}{Abschnitte}
%\crefname{figure}{Abbildung}{Abbildungen}
%\crefname{table}{Tabelle}{Tabellen}
%\crefname{prequisite}{Voraussetzung}{Voraussetzungen}
%\crefname{conclusion}{Folgerung}{Folgerungen}
%\crefname{algorithm}{Algorithmus}{Algorithmen}

\newcommand{\crefrangeconjunction}{ bis }
\newcommand{\crefpairconjunction}{ und }
\newcommand{\crefmiddleconjunction}{, }
\newcommand{\creflastconjunction}{ und }
\newcommand{\crefpairgroupconjunction}{ und }
\newcommand{\crefmiddlegroupconjunction}{, }
\newcommand{\creflastgroupconjunction}{ sowie }


%-------------------------------------------------------
% Macro for the list of requirements
%-------------------------------------------------------

\makeatletter

\newenvironment{reqList}[2][]{%
	\if\relax\detokenize{#1}\relax
	\@ifundefined{c@#2}{\newcounter{#2}\setcounter{#2}{0}}{}
	\def\mylabelstring{#2}
	\def\mylistcnt{#2}
	\else
	\@ifundefined{c@#1}{\newcounter{#1}\setcounter{#1}{0}}{}
	\def\mylabelstring{#2}
	\def\mylistcnt{#1}
	\fi

	\begin{list}{}{%
	\let\makelabel\mylabel
	\setlength{\labelwidth}{6.5em}
	\setlength{\labelsep}{0.5em}
	\setlength{\leftmargin}{6em}
	}
	}{%
	\end{list}
}

\newcommand{\mylabel}[1]{%
\addtocounter{\mylistcnt}{10}
\ifthenelse{\equal{#1}{}}{}{\setcounter{\mylistcnt}{#1}}
\descfont /\mylabelstring~\arabic{\mylistcnt}\hss/}
% New
\newenvironment{reqList2}[1]{\reqChange{#1}\begin{description}}{\end{description}}
% Old
\def\namedlabel#1#2{\begingroup\def\@currentlabel{#2}\label{#1}\endgroup}  % Benanntes Label für Items
\newcommand{\tryToMakeCounter}[1]{\@ifundefined{c@#1}{\newcounter{#1}}{}}  % Erzeuge counter mit Namen aus Parameter, falls dieser noch nicht existiert

\newcommand{\reqName}{}
\newcommand{\reqChange}[1]{\tryToMakeCounter{#1}\renewcommand{\reqName}{#1}}
\newcommand{\reqBegin}[1]{\reqChange{#1}\begin{description}}
\newcommand{\reqItem}[2][\reqName]{\tryToMakeCounter{#1}\addtocounter{#1}{10}\item[/#1\,\arabic{#1}/\namedlabel{#1:#2}{/#1\,\arabic{#1}/}] }
\newcommand{\reqEnd}{\end{description}}
\newcommand{\reqRef}[1]{\textbf{\ref{#1}}}

\makeatother

%-------------------------------------------------------
% Shortcuts
%-------------------------------------------------------
\newcommand{\m}[1]{{\ttfamily #1}}
\newcommand{\q}[1]{\enquote{#1}}
\newcommand{\itnl}[1][1]{\item[\m{#1}] ~\\}
\newcommand{\frontmatter}{\pagenumbering{roman}}
\newcommand{\mainmatter}{\pagenumbering{arabic}}
\newcommand{\<}{\textless}
\renewcommand{\>}{\textgreater\xspace}
\newcommand{\arr}{{[] }}
%-------------------------------------------------------
% Paragraph indention and space between paragraphs
%-------------------------------------------------------

\setlength{\parindent}{1.5em}
\setlength{\parskip}{.3em}

%-------------------------------------------------------
% Make glossaries
%-------------------------------------------------------

\RequirePackage[acronym,toc,section]{glossaries}
\makeglossaries
\addto\captionsngerman{\renewcommand\glossaryname{Glossar}}	% Übersetzung des Section-Titels des Glossars
\deftranslation[to=ngerman]{Acronyms}{Akronymverzeichnis}	% Übersetzung des ToC-Eintrags des Akronymverzeichnisses
\deftranslation[to=ngerman]{Glossary}{Glossar}	% Übersetzung des ToC-Eintrags des Glossars

\input{glossar}

\definecolor{darkgrey}{RGB}{80,80,150}
\renewcommand{\glstextformat}[1]{\textcolor{darkgrey}{#1}\textsuperscript{\includegraphics[width=.75em]{img/link.pdf}}}
